import React from "react";
import Menu from "./common/Menu";
import "../stylesheets/main.scss";
import {NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

// App component
export default class App extends React.Component {

    // render
    render() {
        return (
            <div className="container">
                <div className="row">
                    <Menu/>
                </div>
                <div className="row">
                    {this.props.children}
                </div>
                <div className="row footer"></div>
                <div style={{'width' : '100%', 'minHeight': '50px'}}> </div>
            </div>
        );
    }
}