import {connect} from "react-redux";
import React, { Component, PropTypes } from 'react'
import {Button, ButtonGroup} from "react-bootstrap";

export class Counter extends Component {

    onIncrement = () => {this.props.dispatch({ type: 'INCREMENT' })}
    onDecrement = () => {this.props.dispatch({ type: 'DECREMENT' })}

    incrementIfOdd = () => {
        if (this.props.counter % 2 !== 0) {
            this.onIncrement()
        }
    }

    incrementAsync = () => {
        setTimeout(this.onIncrement, 1000)
    }

    render() {
        return (
            <div className="well">
                Counter: {this.props.counter}
                <br/>
                <br/>
                <ButtonGroup>
                    <Button bsStyle="success" onClick={this.onIncrement}>+</Button>
                    <Button bsStyle="danger" onClick={this.onDecrement}>-</Button>
                    <Button bsStyle="info" onClick={this.incrementIfOdd}>+ (odd)</Button>
                    <Button bsStyle="default" onClick={this.incrementAsync}>+ (1 sec delay)</Button>
                </ButtonGroup>
            </div>
        )
    }
}

// export the connected class
function mapStateToProps(state) {
    return {
        counter: state.counter.value || 0
    };
}

export default connect(mapStateToProps)(Counter);