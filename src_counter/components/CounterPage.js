import React from "react";
import Counter from './common/Counter'

// Counter page component
export default class CounterPage extends React.Component {
    // render
    render() {
        return (
            <div className="page-home">
                <Counter/>
            </div>
        );
    }
}