import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import counter from './counter'

// main reducers
export const reducers = combineReducers({
    routing: routerReducer,
    counter: counter
});