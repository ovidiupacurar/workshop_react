// counter reducer
export default function suggest(state = {}, action) {

    switch (action.type) {
        case 'INCREMENT':
            return Object.assign({}, state, {
                value: (state.value || 0) + 1
            });
        case 'DECREMENT':
            return Object.assign({}, state, {
                value: (state.value || 0) - 1
            });
        default:
            return state
    }
}
